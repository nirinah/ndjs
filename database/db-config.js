
var mongoose = require('mongoose'); 
require('dotenv').config();

var url = process.env.url;

function connected(){
	mongoose.connect(url);
	mongoose.connection.on('connected',()=>{
		console.log('connected in database success'); 
	});

	/*mongoose.connection.once('open',()=>{
		db();
	});*/

	mongoose.connection.on('disconnected',()=>{
	console.log('Disconnected in database:${config.database}'); 
	});

	mongoose.connection.on('error',()=>{
	throw new Error('error connected in database'); 
	});

}

module.exports = connected;
