let conge = require('../../models/conge');
let Employe = require('../../models/employe');
let Contact  = require('../../models/contacts');

module.exports = {
	createConge: function(io,data){
		let addConge;
		let dataSend;

		 Employe.find({'contact':data._id}).then((empl) => { 
		 	addConge = {
		 		dateDebut: data.debut,
    			dateFin: data.fin,
    			duree:data.durre,
    			employe:empl[0]._id
		 	};

			Contact.find({'_id':data._id}).then((contact) => { 
			 	 dataSend = {
			 		dateDebut: data.debut,
	    			dateFin: data.fin,
	    			duree:data.durre,
	    			employe:contact[0].name,
	    			matricule: empl[0].matricule
			 	};
				conge.create(addConge).then(() => { 
					console.log(dataSend)
			        io.emit('congeAdded',dataSend);
		    	}).catch((error) => {
		    		console.log(error.message)
		    	});		
			 });

		 });
	},

	getAllConges(req,res){
		conge.find({}).sort({updateAt: -1}).then((conge)=> {
			let checkData = {};
			if(conge.status){
				checkData = conge;
			} 
			res.json({
	            success : true,
	            code_status : 200,
	            data: checkData
        	}); 
		}).catch((error) => {
    		console.log(error.message)
    	});
	}
}