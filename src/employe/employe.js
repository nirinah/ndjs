let Employe = require('../../models/employe');
let Contact = require('../../models/contacts');


class Employee {
	createEmploye(req,res){
		return Contact.find({'email':req.body.email}).then((contact) => {
	        const data = {
	            'matricule': req.body.matricule,
	            'departement':req.body.departement,
	            'contact':contact[0]
	        };
	        Employe.create(data).then(() => {
	        return res.json(
	                {
	                    success : true,
	                    code_status : 201,
	                    message:"Employer Ajouter avec success"
	                }
	            );
	    	}); 

    	}).catch((error) => {
        	console.log(error);
	        return res.json({
	            success : false,
	            code_status : 404,
	            message: 'user not found'
	        });
    	})
	}

	getAllEmploye(req,res){
		return Employe.find().then((employe)=>{
	        return res.json({
	            success : true,
	            code_status : 200,
	            data: employe
	        })
	    }).catch((error) => {
	            return res.json({
	                success : false,
	                code_status : 404,
	                message: error.message
	            });
	    }) 
	}
}

module.exports = new Employee();
