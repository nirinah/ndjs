let Contact = require('../../models/contacts');
let common = require('../../common/metier');


class Dashboard{
	getDashboardUser(req,res){
		return Contact.findById(req.decoded.id).then((contact)=>{
		 	contact.lastLogin.push(Date.now());
			//Object.assign(contact,lastLogin);
			//console.log(contact)
            contact.save().then(()=>{
	            res.json({
	                success : true,
	                code_status : 200,
	                data: common.transformDataContactById(contact)
	            });
	        }); 
        }).catch((error) => {
        	console.log(error);
	        return res.json({
	            success : false,
	            code_status : 404,
	            message: error.message
	        });
    	})
		
	}
}

module.exports = new Dashboard();