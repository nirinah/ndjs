let Contact  = require('../../models/contacts');
const bcrypt = require('bcryptjs');
let fs       = require('fs');
let path     = require('path');

module.exports = {
	saveContact: function(io,conctAdd){
		console.log(conctAdd)
		let hashPassword    = bcrypt.hashSync(conctAdd.password, 10);
	    let photo           = conctAdd.photo
	    let file            = '';
	    let nameFile;
	    let buf;
	    let affPhoto;

	    if (photo) {
	        if (photo.filetype === 'jpeg') {
	            file = photo.base64.replace(/^data:image\/jpeg;base64,/, "");
	        } else {
	            file  = photo.base64.replace(/^data:image\/png;base64,/, "");
	        }

	        buf = new Buffer(file, 'base64');
	        nameFile = photo.filename+'.'+photo.filetype;

	        fs.writeFile('public/users/'+nameFile,buf,(err) => {
	            if(err) throw err
	                console.log('file à été copier');
	        });
    	}

	    let data = {
	        "name" : conctAdd.name ? conctAdd.name: null,
	        "firstname" : conctAdd.firstname? conctAdd.firstname: null,
	        "raisonSocial" : conctAdd.raisonSocial ? conctAdd.raisonSocial: null,
	        "email": conctAdd.email,
	        "addresse": conctAdd.addresse ? conctAdd.addresse : null,
	        "password": hashPassword,
	        "photo": photo ? path.resolve('public/users')+'/'+nameFile : null,
	        "confirmation_token":null
	    };

	    Contact.create(data).then(() => {
	    	Contact.find({'email':data.email}).then((contact) => {
	    		data._id = contact._id;
	    		if (contact.photo) {
	    			affPhoto:'data:image/jpeg;charset=utf-8;base64,'+ new Buffer(fs.readFileSync(contact.photo)).toString('base64');
	    			data.photo = affPhoto;
	    		}
	    	});
	        io.emit('contactAdded', data);
		}).catch((error)=>{
			console.log(error)
		})
	}
}