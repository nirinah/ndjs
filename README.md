# CRUD applications for contacts Nodejs+ Angular5  HEROKU:
### Managemet of contact the companies

#### What you need to install

* [Node.js](https://nodejs.org/en/)
* [Angular CLI](https://cli.angular.io/)
* [MongoDB](https://www.mongodb.com/)

# How To Start Application?

* Start MongoDB - database mongodb in [mlab](https://mlab.com/)
* Go to [frontend](https://github.com/antonio-nirina/contact-leave-app.git) folder
    * `npm install`
    * `ng serve`
* Go to [backend](https://github.com/antonio-nirina/app-contacts.git) folder
    * `npm install`
    * `gulp develop`

http://localhost:5000/api/contact
    
# License

angular2-node-fb-login is released under [MIT License](https://opensource.org/licenses/MIT).
