let fs      = require('fs');
let path    = require('path');
const bcrypt = require('bcryptjs');

class Metier {

	transformDataContactAll(object){
		let array = [];
        object.forEach( function(elt, index) {
            const row = [];
            let img = fs.readFileSync(elt.photo)
            row.push({
                _id: elt._id,
                name:elt.name,
                firstname: elt.firstname,
                raisonSocial: elt.raisonSocial,
                email:elt.email,
                addresse:elt.addresse,
                photo:'data:image/jpeg;charset=utf-8;base64,'+ new Buffer(img).toString('base64')
            })
            array.push(row);
        });
        let result = array.map((el,i)=>{
           return el[0];
        })

        return result;

	}

	transformDataContactById(obj){
		const row = [];
        let img;
        if (obj.photo) {
            img = fs.readFileSync(obj.photo)
        }
         	row.push({
	                _id: obj._id,
	                name:obj.name?obj.name : null,
	                firstname: obj.firstname ? obj.firstname : null,
	                raisonSocial: obj.raisonSocial ? obj.raisonSocial : null,
	                email:obj.email,
	                addresse:obj.addresse ? obj.addresse : null,
                    nbreConnected: obj.lastLogin,
                    role:obj.role,
	                photo:obj.photo ?'data:image/jpeg;charset=utf-8;base64,'+ new Buffer(img).toString('base64'): null
        		});
        return row;
	}

    postTransformData(obj,image){
        let file     = '';
        let addPhoto = '';
        let nameFile = '';
        let date = new Date();

        if (obj.photo) {
            
            if (obj.photo.filetype === 'jpeg') {
                file = obj.photo.base64.replace(/^data:image\/jpeg;base64,/, "");
                nameFile = obj.photo.filename+'edit'+'.jpeg';
            } else {
                file  = obj.photo.base64.replace(/^data:image\/png;base64,/, "");
                nameFile = obj.photo.filename+'edit'+'.png';
            }

            let buf = new Buffer(file, 'base64');
            fs.writeFile('public/users/'+nameFile,buf,(err) => {
                if(err) throw err
                    console.log('file à été copier');
            });

            addPhoto = path.resolve('public/users')+'/'+nameFile;
        }

        let data = {
            "_id":obj._id,
            "name":obj.name,
            "firstname":obj.firstname,
            "raisonSocial":obj.raisonSocial,
            "email":obj.email,
            "addresse":obj.addresse,
            "photo": obj.photo? addPhoto : image
         };

        return data;
    }

    changePwd(contact, token){
        contact[0].confirmation_token = token;
        return contact;
    }

    contactChangePwd(contact, password){
        contact[0].confirmation_token = null;
        contact[0].password = bcrypt.hashSync(password, 10);
        return contact;
        
    }
      
}

module.exports = new Metier();