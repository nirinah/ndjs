const nodeMailer 	= require('nodemailer');
const bcrypt     	= require('bcryptjs');
let Contact         = require('../models/contacts');
require('dotenv').config();
const PORT 		 	= process.env.port;
const user 		 	= process.env.user;
const pwd 			= process.env.pass;
const host 		 	= process.env.host;
let fs              = require('fs');
let ejs             = require("ejs");
let path            = require('path');



module.exports = {
	sendMailRequest: function(data, res, token){
            nodeMailer.createTestAccount((err,account) => {
            	let transporter = nodeMailer.createTransport({
                host: 'smtp.mailtrap.io',
                port: 2525,
                secure: false, // true for 465, false for other ports
                auth: {
                    user: '18047589e2d07f', // generated ethereal user
                    pass: 'e34552d5fba9b0' // generated ethereal password
                }
            });

            let mailOptions = {};
            let url;
            let opt;
                if (typeof data === 'object') {
                    mailOptions.from = 'Proxima <njacques@bocasay.com>';
                    mailOptions.to = data.email;
                    mailOptions.subject = data.subject;
                    url = 'routes/emails/request.ejs';
                    opt  = { uri:data.uri+'/'+token};
                } else {
                    mailOptions.from = '"Proxima 👻" <njacques@bocasay.com>'; // sender address
                    mailOptions.to =  data;//'bar@example.com, nirinahantonio@gmail.com', // list of receivers
                    mailOptions.subject = 'Hello ✔'; // Subject line
                    mailOptions.text = 'Hello world?'; // plain text body
                    url = 'routes/emails/confirm.ejs';
                    opt  = { uri:'confirmation/'+token};
                }
                ejs.renderFile(path.resolve(url),opt, function (err, result) {
                    if (err) {
                        console.log('erreur:'+err);
                    } else {
                    // send mail with defined transport object
                    mailOptions.html = result;
                        transporter.sendMail(mailOptions).then((email)=>{
                            res.json({
                                success : true,
                                code_status : 200,
                                message: 'un email à été envoyé'
                            })
                        }).catch((error) =>{
                            console.log(error.message)
                        })
                    }
                });
        });
	}
}
