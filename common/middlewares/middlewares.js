const jwt = require('jsonwebtoken');
require('dotenv').config();
let key   = process.env.apisecret;

module.exports = {
	headers : function(req, res, next) {
		res.header("Access-Control-Allow-Origin", "*");
		res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
		res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key,Authorization');
		res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
		
    	next();
	},

	/**
     * Set token credentials to request
     * @param req
     * @param res
     * @param next
     * @returns {*}
     */
    token : function(req, res, next) {
        let token = (req.body && req.body.access_token) || (req.query && req.query.access_token) || req.headers['x-access-token'] || req.headers['authorization'];
        if (token) {
		    jwt.verify(token, key, function(err, decoded) {    
		    	
		      if (err) {
			       	if (err.name === 'TokenExpiredError') {
				        return res.json({ 
				            success: false, 
				            code_status : 401,
				            message: err.message 
				        });    
			      	} else if (JsonWebTokenError === 'JsonWebTokenError') {
				      	return res.json({ 
				            success: false, 
				            code_status : 403,
				            message: err.message 
				        }); 	
		      		}
		      } else {
		        // if everything is good, save to request for use in other routes
		        return req.decoded = decoded;    
		      }
		    });

		} else {
		    return res.json({
	                success : false,
	                code_status : 404,
	                message: 'user not found'
		    });
		}

		next();
    }
}