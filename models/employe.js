var mongoose = require('mongoose'); 

var employeSchema = mongoose.Schema({
    matricule: String, 
    departement:String,
    contact: {type : mongoose.Schema.ObjectId, ref : 'Contact'} 
}); 
 
var Employe = mongoose.model('Employe', employeSchema);

module.exports = Employe;

