var mongoose = require('mongoose'); 

var congeSchema = mongoose.Schema({
    dateDemande:{ type: Date, default: Date.now },
    dateDebut: Date,
    dateFin: Date,
    duree:Number,
    status:{ type: Boolean, default: false },
    employe: {type : mongoose.Schema.ObjectId, ref : 'Employe'} 
}); 
 
var Conge = mongoose.model('Conge', congeSchema);

module.exports = Conge;

