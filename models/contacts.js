let mongoose = require('mongoose'); 
let uniqueValidator = require('mongoose-unique-validator');

var contactSchema = mongoose.Schema({
    name: String, 
    firstname:String,
    raisonSocial: String,
    email : { type: String, index: true, unique: true, required: true },
    addresse: String,
    password: String, 
    confirmation_token:String,
    createdAt:{ type: Date, default: Date.now },
    updateAt:{ type: Date, default: Date.now },
    lastLogin: [],
    photo: String,
    role:{type: String, default: 'user' },
    google: {
        id : {type: String, default: '' },
        token: {type: String, default: '' }
    },
    facebook:{
        id : {type: String, default: '' },
        token: {type: String, default: '' }
    }
    
}); 

contactSchema.plugin(uniqueValidator);
var Contact = mongoose.model('Contact', contactSchema);

module.exports = Contact;