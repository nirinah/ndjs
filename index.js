let express 		  = require('express');
let mongoose 		  = require('mongoose'); 
const PORT 			  = process.env.PORT || 5000;
const app 			  = express();
const middlewares	  = require('./common/middlewares/middlewares');
let http 			  = require('http');
const socket 		  = require('socket.io');
let Botkit            = require('botkit');
const database 		= require('./database/db-config');
const bodyParser 	= require("body-parser"); 
let appRoute        = require('./routes/app');
let contactMetier   = require('./src/contact/contactMetier');
let congeMetier   = require('./src/conge/congeMetier');
let passport        = require('passport');
let config          = require('./config/auth');
require('dotenv').config();



//let redis         = require("redis");
//let client        = redis.createClient();

const router = express.Router();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
// app.use(passport.initialize());
var url = process.env.url;
mongoose.connect(url);
// config.auth(passport, app);

// Create link to Angular build directory
let distDir = __dirname + "/dist/";
app.use(express.static(distDir));
app.all('/api/*', middlewares.headers);
app.use('/api', appRoute);



const server = http.Server(app);
const io  = socket(server);
/*--------------------------------------------
*    socket.io connection
*--------------------------------------------
*/
io.on('connection', (socket) => {
    // Log whenever a user connects
    console.log(socket.id);
    socket.on('disconnect', function(){
        console.log('user disconnected');
    }); 

    // When we receive a 'message' event from our client, print out
    // the contents of that message and then echo it back to our client
    // using `io.emit()`
    socket.on('message', (message) => {
        console.log("Message Received: " + message);
        io.emit('message', {type:'new-message', text: message});    
    });

    socket.on('addContact', (contact)=>{
        console.log(contact)
        contactMetier.saveContact(io,contact);
    })

    socket.on('addConge', (conge) => {
        congeMetier.createConge(io,conge);
    })
});


/*------------------------------------------
* Connecte Redis
*-----------------------------------------
*/

//client.set("test", "string val", redis.print);

server.listen(PORT, () => console.log(`Listening on ${ PORT }`));


