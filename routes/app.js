require('dotenv').config();
const express           = require('express');
const router            = express.Router();
let Contact             = require('../models/contacts');
let Employe             = require('../models/employe');
const bcrypt            = require('bcryptjs');
let apiKey              = process.env.MAILJET_APIKEY;
let apiSecret           = process.env.MAILJET_APISECRET;
let jwt                 = require('jsonwebtoken');
let mailjet             = require ('node-mailjet').connect(apiKey,apiSecret);
const middlewares       = require('../common/middlewares/middlewares');
const nodeMailer        = require('nodemailer');
let fs                  = require('fs');
const metierEmploye     = require('../src/employe/employe');
const metierDashboard   = require('../src/dashboard/dashboard');
let path                = require('path');
let pdf                 = require('html-pdf');               
let metier              = require('../common/metier');
let socket              = require('socket.io');
let request             = require('request');
const axios             = require('axios');
const mailer            = require('../common/mailer');
const EventEmitter      = require('events');
let key                 = process.env.apisecret;
let passport            = require('passport');

const url =`https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=40.6655101,
 -73.89188969999998&destinations=40.6905615%2C-73.9976592%7C40.6905615
 %2C-73.9976592%7C40.6905615%2C-73.9976592%7C40.6905615%2C-73.9976592%7C40.6905615%2C-73.9976592%7C40.6905615%
 2C-73.9976592%7C40.659569%2C-73.933783%7C40.729029%2C-73.851524%7C40.6860072%2C-73.6334271%7C40.598566%2C-73.7527626%7C40.659569
 %2C-73.933783%7C40.729029%2C-73.851524%7C40.6860072%2C-73.6334271%7C40.598566%2C-73.7527626&key=AIzaSyAbx1eDKJpMFAS4bPm3SQWXowXUfwZB594`;

/**
*---------------------------------------------
* Socket Io connected
*--------------------------------------------
*/


/**
*---------------------------------
*
*CREATE PDF
*
*--------------------------------
*
*/

router.get('/pdf/create',(req,res) => {
    let html = fs.readFileSync('view/pdf.ejs', 'utf8');
    var options = { format: 'Letter' };
    /*-----------------------------------------------
    * return directly PDF
    *-------------------------------------------------
    */
    pdf.create(html).toStream(function(err, stream){
       if (err) return res.send(err);
        res.type('pdf');
        stream.pipe(res);
    });
    /*-----------------------------------------------
    * return stream Bynary
    *-------------------------------------------------
    */

    /*pdf.create(html, options).toFile('./file.pdf', function(err, res) {
        if (err) return console.log(err);
        console.log(res); // { filename: '/file.pdf' }
    });*/
    
});

router.get('/test',(req,res) => {
    /*fs.readFileSync('./index.html', 'utf-8', function(error, content) {
        res.writeHead(200, {"Content-Type": "text/html"});
        res.end(content);
    });*/
    const myEmitter = new EventEmitter();
    myEmitter.on('event', () => {
      console.log('an event occurred!');
    });

    axios.get(url).then(resp => {
        console.log(resp.data)
        res.send(
                {
                    success : true,
                    status:200,
                    data:resp.data
                }
        );
    }).catch(err => {
        console.log(err)
    })

});

/**
*---------------------------------
*
*Google Authentication
*
*--------------------------------
*
*/

router.get('/auth/google', passport.authenticate(
    'google', { scope : ['profile', 'email'] }));

// the callback after google has authenticated the user
router.get('/auth/google/callback',
    passport.authenticate('google', { failureRedirect: '/login' }),
    function(req, res) {
        console.log(res)
        res.redirect('http://localhost:4200/dashbord/user');
    } 
);

router.post('/login',(req,res) => {
    let identifier;
    const auth = req.body.username;

    if (!auth.match('@')) {
        //identifier.name = req.body.username;
        identifier = {'name':req.body.username};
    } else {
        //identifier.email = req.body.username;
        identifier = {'email':req.body.username};
    }

    Contact.find(identifier).then((contact) => {
            const checkPwd = bcrypt.compareSync(req.body.password,contact[0].password);
            const payload = {
                id:contact[0]._id,
                role:contact[0].role,
                email:contact[0].email
            };

            if (checkPwd && contact[0].confirmation_token == null) {
                jwt.sign(payload, key, {expiresIn: 60*60*24 }, (err, token) => {
                    //console.log(jwt.decode(token))
                    res.json({
                        success : true,
                        code_status : 200,
                        data: token
                    }); 
                });
            } else {
                res.json({
                        success : false,
                        code_status : 401,
                        message: 'Veuillez confirmer votre compte'
                });
            }
    }).catch((error) => {
        console.log(error);
        res.json({
            success : false,
            code_status : 404,
            message: 'user not found'
        });
    })
});

router.post('/request-password',(req,res) => {
    Contact.find({'email':req.body.email}).then((contact) => {
        if (contact.length > 0) {
            let data     = {};
            data.email   = req.body.email;
            data.subject = 'forgot password';
            data.uri     = req.body.uri;
            let token    = bcrypt.genSaltSync(50);
            let newToken = token.match('/\//') ? token.replace('/\//', token): token;
            contactModif =  metier.changePwd(contact,newToken);
            Object.assign(contact,contactModif);
            contact[0].save().then((success) => {
                mailer.sendMailRequest(data,res, token);
            });
           
        } else {
            res.json({
                success : false,
                code_status : 404,
                message: 'email not found'
            }); 
        }
       
    }).catch((error) => {
        res.json({
            success : false,
            code_status : 500,
            message: 'error Interne'
        });
    })
});

router.post('/reset-password',(req,res) => {
      Contact.find({'confirmation_token':req.body.token}).then((contact) => {
         contactChangePwd =  metier.contactChangePwd(contact,req.body.password);
            Object.assign(contact,contactChangePwd);
            contact[0].save().then(()=>{
               res.json({
                    success : true,
                    code_status : 200,
                    message: 'Mot de passe à été modifier avec success'
                })
            });
      }).catch((error) => {
        res.json({
            success : false,
            code_status : 500,
            message: 'error Interne'
        });
    })
});

/**
*---------------------------------
*
*CREATE Contact
*
*--------------------------------
*
*/

router.post('/contact',(req,res) => {
    let conctAdd        = req.body;
    let hashPassword    = bcrypt.hashSync(conctAdd.password, 10);
    let photo           = conctAdd.photo
    let file            = '';
    let nameFile;
    let buf;

    if (photo) {
        if (photo.filetype === 'jpeg') {
            file = photo.base64.replace(/^data:image\/jpeg;base64,/, "");
        } else {
            file  = photo.base64.replace(/^data:image\/png;base64,/, "");
        }

        buf = new Buffer(file, 'base64');
        nameFile = photo.filename+'.'+photo.filetype;

        fs.writeFile('public/users/'+nameFile,buf,(err) => {
            if(err) throw err
                console.log('file à été copier');
        });
    }

    let data = {
        "name" : conctAdd.name ? conctAdd.name: null,
        "firstname" : conctAdd.firstname? conctAdd.firstname: null,
        "raisonSocial" : conctAdd.raisonSocial ? conctAdd.raisonSocial: null,
        "email": conctAdd.email,
        "addresse": conctAdd.addresse ? conctAdd.addresse : null,
        "password": hashPassword,
        "photo": photo ? path.resolve('public/users')+'/'+nameFile : null,
        "confirmation_token":null
    };

    Contact.create(data).then(() => { 
        if (conctAdd.sendMail) {
            mailer.sendMail(conctAdd.email);
        }

        res.json(
                {
                    success : true,
                    code_status : 201,
                    message:"Contact Ajouter avec success"
                }
            );
    }); 
});

router.use(middlewares.token);

router.get('/contact',(req,res) => {
    Contact.find({}).sort({updateAt: -1}).then((contact)=> {
        let array  = [];
        let result = [];
        let img = '';
        contact.forEach( function(elt, index) {
            const row = [];

            if (elt.photo) {
                img = fs.readFileSync(elt.photo)
            } 
            
            row.push({
                _id: elt._id,
                name:elt.name,
                firstname: elt.firstname,
                raisonSocial: elt.raisonSocial,
                email:elt.email,
                addresse:elt.addresse,
                role:elt.role,
                photo: elt.photo ?'data:image/jpeg;charset=utf-8;base64,'+ new Buffer(img).toString('base64'): null
            })
            array.push(row);
        });
         result = array.map((el,i)=>{
           return el[0];
        })

        res.json({
            success : true,
            code_status : 200,
            data: result
        }); 
    });
});

router.get('/contact/:_id',(req,res)=>{
        Contact.find(req.params).then((contact) => {
            res.json({
                success : true,
                code_status : 200,
                data: contact
            }); 
        });
});

router.put('/contact',(req,res)=>{
        const data  = req.body;
        Contact.findById(data._id).then(dbContact =>{
            contact =  metier.postTransformData(data, dbContact.photo);
            Object.assign(dbContact,contact);
            dbContact.save().then(()=>{
                res.json(
                    {
                        success : true,
                        status:200,
                        message:"Modification avec success"
                    }
                );
            });
        }).catch((err) => {
            res.json(
                {
                    success : false,
                    status:403,
                    message:err.message
                }
            )
        });
});

router.delete('/contact/:_id',(req,res)=>{
        Contact.deleteOne(req.params).then(()=>{
            res.json(
                    {
                        success : true,
                        status:200,
                        message:"Suppression avec success"
                    }
                );
        });
        
});

router.post('/emailing',(req,res) =>{
    const id = req.decoded.id;
     Contact.findById(id).then(user => {
        let sendMail  = mailjet.post("send");
        let emailData = {
            'FromEmail': user.email,
            'FromName': user.name,
            'Subject': req.body.sujet,
            'Text-part': req.body.message,
            'Recipients': [{'Email': req.body.email}]
        }

        if(typeof (req.body.attach) !== 'undefined'){
            emailData.Attachments = [{
                "Content-Type": req.body.attach.filetype,
                "Filename": req.body.attach.filename,
                "Content": req.body.attach.value, // Base64 for "This is your attached file!!!"
            }];
        }

    //sendMail.request(emailData).then(handlePostResponse).catch(handleError);
        sendMail.request(emailData).then(data => {
            console.log(data);
            return res.json(
                {
                    success : true,
                    status:200,
                    message:"Mail envoyé avec success"
                }
            );

        }).catch();
    });

    /*if (sendMail.callUrl == 'send') {
        res.json(
            {
                success : true,
                status:200,
                message:"Mail envoyé avec success"
            }
        );
    } else {
        res.json(
            {
                success : false,
                status:401,
                message:"Mail not send"
            }
        );
    }*/
});

router.post('/employe',(req,res) => {
    metierEmploye.createEmploye(req,res);
});

router.get('/employe',(req,res) => {
    metierEmploye.getAllEmploye(req,res);
});

router.get('/dashbord',(req,res) => {
    metierDashboard.getDashboardUser(req,res);
});

router.get('/conge',(req,res) => {
    congeMetier.getAllConges(req,res);
});

module.exports = router;