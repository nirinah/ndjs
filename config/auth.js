let FacebookStrategy 	= require('passport-facebook').Strategy;
let GoogleStrategy   	= require('passport-google-oauth').OAuth2Strategy;
let Contact          	= require('../models/contacts');

require('dotenv').config();

module.exports = {
	auth: function(passport, app) {
		let url;
		passport.serializeUser(function(contact, done) {
			if (contact) {
				done(null, contact.id);
			} else {
				window.location.replace('http://localhost:4200/login');
			}
        	
    	});

    	passport.deserializeUser(function(id, done) {
	        Contact.findById(id, function(err, user) {
	            done(err, user);
	        });
    	});

    	if (app.get('env') == 'development') {
    		url = process.env.callbackURLG;
    	} else {
    		url = process.env.callbackURLGProd;
    	}

    	passport.use(new GoogleStrategy({
	        clientID        : process.env.clientIDG ,
	        clientSecret    : process.env.clientSecretG,
	        callbackURL     : url

    	},
			function(token, refreshToken, profile, done) {
	        process.nextTick(function() {
	            Contact.findOne({'google.id' : profile.id }, function(err, user) {
	                if (err) return done(err);

	                if (user) {
	                    // if a user is found, log them in
	                    return done(null, user);
	                } else {
	                    var newUser     = new Contact();
	                    newUser.firstname = null;
	                    newUser.raisonSocial = null ;
	                    newUser.addresse = null ;
	                    newUser.password = null ;
	                    newUser.confirmation_token = null ;
	                    newUser.photo = null;
	                    newUser.google.id    = profile.id;
	                    newUser.google.token = token;
	                    newUser.name  = profile.displayName;
	                    newUser.email = profile.emails[0].value; // pull the first email

	                    // save the user
	                    newUser.save(function(err) {
	                        if (err)
	                            throw err;
	                        return done(null, newUser);
	                    });
	                }
	            });
	        });

    	}));

	}
}

